packer {
    required_plugins {
        proxmox = {
            version = ">= 1.1.3"
            source  = "github.com/hashicorp/proxmox"
        }
    }
}


variable "proxmox_api_url" {
    type = string
}

variable "proxmox_api_token_id" {
    type = string
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
}

variable "vm_id" {
    type = string
}

variable "vm_name" {
    type = string
}

variable "template_description" {
    type = string
}

variable "proxmox_node" {
    type = string
    default = "pve"
}

variable "storage_pool" {
    type = string
    default = "local-zfs"
}

source "proxmox-iso" "tmpl_rocky-9-server-base" {
    proxmox_url = "${var.proxmox_api_url}"
    username = "${var.proxmox_api_token_id}"
    token = "${var.proxmox_api_token_secret}"
    insecure_skip_tls_verify = true

    node = "${var.proxmox_node}"
    vm_id = "${var.vm_id}"
    vm_name = "${var.vm_name}"
    template_description = "${var.template_description}"

    iso_file = "ISO:iso/Rocky-9.2-x86_64-dvd.iso"

    iso_storage_pool = "ISO"
    unmount_iso = true

    qemu_agent = true

    scsi_controller = "virtio-scsi-pci"

    disks {
        storage_pool = "${var.storage_pool}"
        disk_size = "16G"
    }

    cores = "1"
    cpu_type = "host"

    memory = "1024"

    network_adapters {
        model = "virtio"
        bridge = "vmbr0"
        firewall = "false"
    }

    cloud_init = true
    cloud_init_storage_pool = "${var.storage_pool}"

    boot_command = [
        "<tab><wait>",
        "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
        "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
        " text inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/inst.ks<enter><wait>"]
    boot_wait = "5s"

    http_directory = "http" 

    ssh_timeout = "30m"
    ssh_username = "root"
    ssh_password = "Packer"
}

build {

    name = "rocky-9-test-1"
    sources = ["source.proxmox-iso.tmpl_rocky-9-server-base"]

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #1
    provisioner "shell" {
        inline = [
            "dnf install -y cloud-init qemu-guest-agent cloud-utils-growpart gdisk",
            "systemctl enable qemu-guest-agent",
            "shred -u /etc/ssh/*_key /etc/ssh/*_key.pub",
            "rm -f /var/run/utmp",
            ">/var/log/lastlog",
            ">/var/log/wtmp",
            ">/var/log/btmp",
            "rm -rf /tmp/* /var/tmp/*",
            "unset HISTFILE; rm -rf /home/*/.*history /root/.*history",
            "rm -f /root/*ks",
            "passwd -d root",
            "passwd -l root",
            "rm -f /etc/ssh/ssh_config.d/allow-root-ssh.conf"
        ]
    }

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #2
    provisioner "file" {
        source = "files/99-pve.cfg"
        destination = "/tmp/99-pve.cfg"
    }

    # Provisioning the VM Template for Cloud-Init Integration in Proxmox #3
    provisioner "shell" {
        inline = [ "sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg" ]
    }
}